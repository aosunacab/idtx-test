FROM adoptopenjdk/openjdk11:alpine-jre

EXPOSE 8081

ADD target/prices-service-0.1.0-SNAPSHOT.jar /usr/share/app.jar
ENTRYPOINT ["java", "-jar", "/usr/share/app.jar"]