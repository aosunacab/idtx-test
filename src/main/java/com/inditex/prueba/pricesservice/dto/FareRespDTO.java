package com.inditex.prueba.pricesservice.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inditex.prueba.pricesservice.utils.BigDecimalSerialize;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FareRespDTO {
        
    @JsonProperty(value = "product_id", required = true)
    private Integer productId;
    
    @JsonProperty(value = "brand_id", required = true)
    private Short brandId;
    
    @JsonProperty(value = "price_list", required = true)
    private int priceList;
    
    @JsonProperty(value = "start_date", required = true)
    private LocalDateTime startDate;
    
    @JsonProperty(value = "end_date", required = true)
    private LocalDateTime endDate;
    
    @JsonProperty(value = "price", required = true)
    @JsonSerialize(using = BigDecimalSerialize.class)
    private BigDecimal price;
    
}
