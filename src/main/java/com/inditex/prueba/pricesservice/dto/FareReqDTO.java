package com.inditex.prueba.pricesservice.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = false)
public class FareReqDTO {
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH.mm.ss")
    @JsonProperty(value= "application_date", required = true)
    @NotNull
    private LocalDateTime date;
    
    @JsonProperty(value= "product_id", required = true)
    @NotNull
    private Integer productId;
    
    @JsonProperty(value= "brand_id", required = true)
    @NotNull
    private Short brandId;

}
