package com.inditex.prueba.pricesservice.controller;

import java.time.LocalDateTime;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.MappingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.inditex.prueba.pricesservice.controller.error.CustomErrorResponse;

@ControllerAdvice
public class PricesControllerAdvice {

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<CustomErrorResponse> internalNullPointer(NullPointerException ex) {
        CustomErrorResponse error = new CustomErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setError(ex.getMessage());
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ EntityNotFoundException.class, IndexOutOfBoundsException.class })
    public ResponseEntity<CustomErrorResponse> notFound(Exception ex) {
        CustomErrorResponse error = new CustomErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setError("Data not found with the input request");
        error.setStatus(HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MappingException.class)
    public ResponseEntity<CustomErrorResponse> mappingError(MappingException ex) {
        CustomErrorResponse error = new CustomErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setError("Something went really wrong :(");
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
