package com.inditex.prueba.pricesservice.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inditex.prueba.pricesservice.dto.FareReqDTO;
import com.inditex.prueba.pricesservice.dto.FareRespDTO;
import com.inditex.prueba.pricesservice.model.Prices;
import com.inditex.prueba.pricesservice.service.PricesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/fares")
public class PricesController {
    
    @Autowired
    private PricesService pricesService;
    
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FareRespDTO> getCurrentFareForm(@RequestParam MultiValueMap<String, String> form) {
        FareReqDTO fareRequest = new FareReqDTO();
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss"); 
            LocalDateTime dateTime = LocalDateTime.parse(form.getFirst("application_date"), formatter);        
            Integer productId = Integer.valueOf(form.getFirst("product_id"));
            Short brandId = Short.valueOf(form.getFirst("brand_id"));
            
            fareRequest = new FareReqDTO(dateTime, productId, brandId);
        } catch (Exception e) {
            log.error("Input data error: ", e);
            return ResponseEntity.badRequest().build();
        }        
        
        List<Prices> price = this.pricesService.getCurrentFare(fareRequest);
        
        FareRespDTO fareResp = this.modelMapper.map(price.get(0), FareRespDTO.class);
        
        return ResponseEntity.ok(fareResp);
    }
    
    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FareRespDTO> getCurrentFareJson(@Valid @RequestBody FareReqDTO request) {   
        
        List<Prices> price = this.pricesService.getCurrentFare(request);
        FareRespDTO fareResp = this.modelMapper.map(price.get(0), FareRespDTO.class);
        
        return ResponseEntity.ok(fareResp);
    }
    
}
