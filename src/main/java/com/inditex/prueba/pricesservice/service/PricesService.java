package com.inditex.prueba.pricesservice.service;

import java.util.List;

import com.inditex.prueba.pricesservice.dto.FareReqDTO;
import com.inditex.prueba.pricesservice.model.Prices;

public interface PricesService {

    /**
     * 
     * Get the current fare (prices) from certain input
     * attributes. 
     * 
     * @param fareRequest
     * @return list of prices found
     */
    public List<Prices> getCurrentFare(FareReqDTO fareRequest);
}
