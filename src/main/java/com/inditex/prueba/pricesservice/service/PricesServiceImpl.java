package com.inditex.prueba.pricesservice.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.inditex.prueba.pricesservice.dto.FareReqDTO;
import com.inditex.prueba.pricesservice.model.Prices;
import com.inditex.prueba.pricesservice.repository.PricesRepository;
import com.inditex.prueba.pricesservice.repository.PricesSpecifications;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PricesServiceImpl implements PricesService {

    @Autowired
    private PricesRepository pricesRepository;
    
    @Override
    public List<Prices> getCurrentFare(FareReqDTO fareRequest) {
        try {
            return this.pricesRepository.findAll(Specification.where(
                    PricesSpecifications.betweenDatesPrioritized(fareRequest.getDate())
                        .and(PricesSpecifications.isBrand(fareRequest.getBrandId())
                        .and(PricesSpecifications.isProduct(fareRequest.getProductId())))));
        } catch (Exception e) {
            log.error("Error accessing data: ", e);
            throw new EntityNotFoundException();
        }
        
    }

}
