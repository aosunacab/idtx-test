package com.inditex.prueba.pricesservice.repository;


import java.time.LocalDateTime;

import javax.persistence.criteria.Join;

import org.springframework.data.jpa.domain.Specification;

import com.inditex.prueba.pricesservice.model.Brands;
import com.inditex.prueba.pricesservice.model.Brands_;
import com.inditex.prueba.pricesservice.model.Prices;
import com.inditex.prueba.pricesservice.model.Prices_;

/**
 * Class with predefined persistence criteria methods
 * 
 * @author AntonioOsuna
 *
 */
public class PricesSpecifications {
    
    public static Specification<Prices> isProduct(Integer productId) {
        return (root, query, cb) -> cb.equal(root.get(Prices_.productId), productId);
    }
    
    public static Specification<Prices> isBrand(Short brandId) {
        return (root, query, cb) -> {
            Join<Prices, Brands> join = root.join(Prices_.brand);
            return cb.equal(join.get(Brands_.id), brandId);
        };
    }
    
    public static Specification<Prices> lessEqDate(LocalDateTime date) {
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get(Prices_.startDate), date);
    }
    
    public static Specification<Prices> greaterEqDate(LocalDateTime date) {
        return (root, query, cb) -> cb.greaterThanOrEqualTo(root.get(Prices_.endDate), date);
    }
    
    public static Specification<Prices> prioritized(Specification<Prices> specification) {
        return (root, query, cb) -> {
            query.orderBy(cb.desc(root.get(Prices_.priority)));
            return specification.toPredicate(root, query, cb);
        };
    }
    
    public static Specification<Prices> betweenDatesPrioritized(LocalDateTime date) {        
        return prioritized(Specification.where(lessEqDate(date)).and(greaterEqDate(date)));
    }
}
