package com.inditex.prueba.pricesservice.repository;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.inditex.prueba.pricesservice.model.Prices;

@Repository
public interface PricesRepository extends CrudRepository<Prices, Long>, JpaSpecificationExecutor<Prices> {
    List<Prices> findAll(Specification<Prices> spec);
}
