package com.inditex.prueba.pricesservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Table
@Entity
@Data
public class Brands {

    @Id
    private Short id;
    @Column
    private String name;
    
}
