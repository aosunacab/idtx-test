package com.inditex.prueba.pricesservice.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Table(name = "prices")
@Data
public class Prices {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
        
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brand_id", referencedColumnName = "id")
    private Brands brand;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH.mm.ss")
    @Column(name = "start_date", columnDefinition = "SMALLDATETIME")
    private LocalDateTime startDate;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH.mm.ss")
    @Column(name = "end_date", columnDefinition = "SMALLDATETIME")
    private LocalDateTime endDate;
    
    @Column(name = "price_list")
    private int priceList;
    
    @Column(name = "product_id")
    private Integer productId;
    
    @Column(name = "priority")
    private int priority;
    
    @Column(name = "price")
    private double price;
    
    @Column(name = "curr")
    private String currency;    
    
}
