package com.inditex.prueba.pricesservice.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.util.ObjectUtils;

public class BigDecimalSerialize extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializerProvider)
            throws IOException {
        if (!ObjectUtils.isEmpty(value)) {
            gen.writeString(value.setScale(2, RoundingMode.HALF_EVEN) + "");
        } else {
            gen.writeString(value + "");
        }
    }
}