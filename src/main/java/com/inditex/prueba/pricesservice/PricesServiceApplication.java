package com.inditex.prueba.pricesservice;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class PricesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricesServiceApplication.class, args);
	}

	@Bean
    ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
