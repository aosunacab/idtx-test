package com.inditex.prueba.pricesservice;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.inditex.prueba.pricesservice.dto.FareReqDTO;

@SpringBootTest
@AutoConfigureMockMvc
class PricesServiceApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss");
    private ObjectMapper om = JsonMapper.builder().addModule(new JavaTimeModule()).build();
    
    
	@Test
	void testCase1() throws Exception {
	    FareReqDTO request = new FareReqDTO(LocalDateTime.parse("2020-06-14 10.00.00", this.formatter), 35455, (short)1);
	    
	    makeCall(request, "35.50");
	}
	
	@Test
    void testCase2() throws Exception {
        FareReqDTO request = new FareReqDTO(LocalDateTime.parse("2020-06-14 16.00.00", this.formatter), 35455, (short)1);
        
        makeCall(request, "25.45");
    }
	
	@Test
    void testCase3() throws Exception {
        FareReqDTO request = new FareReqDTO(LocalDateTime.parse("2020-06-14 21.00.00", this.formatter), 35455, (short)1);
        
        makeCall(request, "35.50");
    }
	
	@Test
    void testCase4() throws Exception {
        FareReqDTO request = new FareReqDTO(LocalDateTime.parse("2020-06-15 10.00.00", this.formatter), 35455, (short)1);
        
        makeCall(request, "30.50");
    }
	
	@Test
    void testCase5() throws Exception {
        FareReqDTO request = new FareReqDTO(LocalDateTime.parse("2020-06-16 21.00.00", this.formatter), 35455, (short)1);
        
        makeCall(request, "38.95");
    }
	
	private void makeCall(FareReqDTO request, String priceExpected) throws Exception {
	    ObjectWriter ow = this.om.writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(request);
        
        this.mockMvc.perform(post("/fares/")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(json))
            .andDo(print()).andExpect(status().isOk())
            .andExpect(content().string(containsString(priceExpected)));
	}

}
