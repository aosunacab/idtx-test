
# IDTX - TEST - SERVICES

Inditex knowledge test. 

Basic REST service Spring boot.

## Usage
### Build

First, compile and package the project from the parent folder:
```
mvn clean package
```
A docker structure has been created. For build the image locally and run the container execute the following command:
```
docker-compose up
```
### Configuration
You can find the basic configuration at `~/resources/application.properties`

### API
* `http://localhost:8081/fares/`:
    - Mehotd: **POST**
	- Accept **JSON** body as following:
	```json
    {
    "application_date": "2020-06-15 21.00.00",
    "product_id": "35455",
    "brand_id": "1"
    }
    ```
    - Return JSON body with the corresponding information
    - HTTP handled responses:
        - 200 - OK
        - 400 - Bad Request
        - 404 - Not found
        - 500 - Internal error
	
